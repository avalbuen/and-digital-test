import { Component, OnInit } from '@angular/core';
import { HackernewServiceService } from '../services/hackernew-service.service';
import { IHackernewsTitle } from '../services/hackernwes.interface';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [HackernewServiceService]
})
export class AppComponent {
  hackernewsTitle: IHackernewsTitle = null;
  hackernewsLates: Array<number>;
  panelOpenState = false;
  constructor(
    private HackernewService: HackernewServiceService,
    private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    this.HackernewService.getLatesHackrnes().subscribe(x => {
      this.hackernewsLates = x;
    });
  }
  selectedHackernews(id: number) {
    this.spinner.show();
      this.HackernewService.getHackernewTitle(id).subscribe(y => {
        this.hackernewsTitle = y;
        this.spinner.hide();
      });
  }
}
