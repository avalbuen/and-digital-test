import { TestBed } from '@angular/core/testing';

import { HackernewServiceService } from './hackernew-service.service';

describe('HackernewServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HackernewServiceService = TestBed.get(HackernewServiceService);
    expect(service).toBeTruthy();
  });
});
