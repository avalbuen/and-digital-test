import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IHackernewsTitle } from './hackernwes.interface';

@Injectable({
  providedIn: 'root'
})
export class HackernewServiceService {

  constructor(private http: HttpClient) { }

  getLatesHackrnes(): Observable<any> {
    return this.http.get<any>('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty');
  }

  getHackernewTitle(id: number): Observable<IHackernewsTitle> {
    return this.http.get<IHackernewsTitle>('https://hacker-news.firebaseio.com/v0/item/' + id + '.json?print=pretty');
  }
}
